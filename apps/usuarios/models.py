from django.db import models

# Create your models here.
class Usuarios(models.Model):

    

    nombre = models.CharField(max_length=15)
    apellidos = models.CharField(max_length=15)
    cc= models.IntegerField(unique=True) # AQUÍ
    cargo = models.CharField(max_length=15)
    email = models.EmailField()
    telefono = models.CharField(max_length=12)
   

    def __str__(self):
        return '%s'% (self.nombre)

        #falta devolver el cc  -UUID-
from django.urls import path
from . import views

app_name = 'orderUser'

urlpatterns = [
    path('', views.order_to_user, name='orderUser')
]
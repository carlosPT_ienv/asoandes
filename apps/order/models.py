from django.db import models

from apps.usuarios.models import Usuarios
from apps.orderItem.models import OrderItem
# Create your models here.
class Order(models.Model):
    cod_order = models.CharField(max_length=15)
    cod_usr = models.ForeignKey(Usuarios, on_delete=models.SET_NULL, null=True)
    items = models.ForeignKey(OrderItem, on_delete=models.SET_NULL, null=True)
    date_ordered = models.DateTimeField(auto_now=True)

    def get_cart_items(self):
        return self.items.all()

    def __str__(self):
        return '{0} - {1}'.format(self.owner, self.ref_code)
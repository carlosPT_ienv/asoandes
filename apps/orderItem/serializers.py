# from rest_framework import serializers
from rest_framework import serializers
from apps.orderItem.models import OrderItem

# # from apps.orderItem.serializers import


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = "__all__"
        


from django.db import models

# from __future__ import unicode_literals

from django.db import models

from apps.productos.models import Producto


class OrderItem(models.Model):
    # ManyToOneField  OneToManyField
    # cantidad= models.IntegerField(null=True)
    # Precio unitario
    # Precio total
    name = models.CharField(max_length=200, null=False)
    price = models.IntegerField()
    quantity = models.IntegerField(default="1")
    product = models.ForeignKey(Producto, on_delete=models.SET_NULL, null=True)
    # fecha = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name

    def get_item_total(self):
        return self.product.price * self.cantidad


# Create your models here.

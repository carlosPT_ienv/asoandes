from django import forms
from apps.productos.models import Producto
from apps.orderItem.models import OrderItem

class productoForm(forms.ModelForm):
    #product =  forms.ModelChoiceField(queryset=Producto.objects.all())
    #cantidad = forms.IntegerField()

    #data = {
    #    'producto': product,
    #    'cantidad': cantidad,
    #}
    #form = productoForm(data)
    class Meta:
        model = OrderItem
        fields = ['product', 'cantidad']

    def __init__(self, *args, **kwargs):
        super(OrderItem, self).__init__(*args, **kwargs)
        self.product = Producto.objects.get(id=kwargs['initial']['pk'])

        for field in iter(self.fields):
            if field != 'estado':
                self.fields[field].widget.attrs.update({
                    'class': 'form-control'
                })
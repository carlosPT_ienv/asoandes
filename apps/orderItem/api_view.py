from rest_framework.response import Response
from rest_framework import status
from .serializers import OrderItemSerializer
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt, csrf_protect


class OrderItemAPI(APIView):
    # @csrf_protect
    @csrf_exempt
    def post(self, request):
        # print(request.data)
        oi_data = OrderItemSerializer(data=request.data)

        print(oi_data)
        if oi_data.is_valid():
            orderItem = oi_data.save()
            print(orderItem)
            return Response(oi_data.data, status=status.HTTP_201_CREATED)
        else:
            return Response(oi_data.data, status=status.HTTP_400_BAD_REQUEST)

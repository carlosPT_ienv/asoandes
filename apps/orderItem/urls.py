from django.urls import path
from . import views, api_view

app_name = "orderItem"

urlpatterns = [
    path('', api_view.OrderItemAPI.as_view(), name='orderItem'),
    path('ordenes', views.OrderItemView.as_view(), name='orderItem'),

    # path('/(?P<pk>.+)/', views.add_item, name='orderItem')                 (?P<pk>\d+)
]


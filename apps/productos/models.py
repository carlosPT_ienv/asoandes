from django.db import models

# -*- coding: utf-8 -*-
#from __future__ import unicode_literals
from django.db import models

CATEGORY_CHOICES = (
    ("HORTALIZAS", "HORTALIZAS"),
    ("AROMÁTICAS", "AROMÁTICAS"),
    ("TUBERCULOS-RAICES", "TUBERCULOS-RAICES"),
    ("MUSACEAS", "MUSACEAS"),
    ("FRUTALES", "FRUTALES"),
    ("CEREALES", "CEREALES"),
    ("LEGUMINOSAS", "LEGUMINOSAS"),
    ("CONCENTRADOS-FERTILIZANTES", "CONCENTRADOS-FERTILIZANTES"),
    ("ELEMENTOS-HUERTA", "ELEMENTOS-HUERTA"),
)

class Producto(models.Model):
    id=models.AutoField(primary_key=True)
    category= models.CharField(max_length=120,choices = CATEGORY_CHOICES, default = '1')
    unid_medida=models.CharField(max_length=120,null=True)
    name = models.CharField(max_length=120)
    price = models.IntegerField()

    def __str__(self):
        return self.name

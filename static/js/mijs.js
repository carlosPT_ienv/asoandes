// Product Constructor
class Product {
    constructor(name, price, quantity, total) {
        this.name = name;
        this.price = price;
        // this.year = year;
        this.quantity = quantity;
        this.total = total
    }

}

// UI Constructor
class UI {
    // addProduct(product) {
    //     const productList = document.getElementById('product-list');
    //     const element = document.createElement('div');
    //     // element.className = "card text-center md-12 mt-2"
    //     element.innerHTML = `

    //             <div id="list" class="card-body" data-total="${product.total}">
    //                 <strong>Product</strong>: ${product.name} -
    //                 <strong>Price</strong>: ${product.price} - 
    //                 <!--<strong>Year</strong>: ${product.year} --->
    //                 <strong>Quantity</strong>: ${product.quantity} -
    //                 <div class="quantity">
    //                 <input type="number" min="1" max="15" value="1" readonly>
    //                 </div>
    //                 <div class="quantity-nav">
    //                     <div class="quantity-button quantity-up" disabled="false">+</div>
    //                     <div class="quantity-button quantity-down" disabled="false">-</div>
    //                 </div>
    //                 <strong>Total</strong>: ${product.total}
    //                 <a href="#" class="btn btn-danger" name="delete">Delete</a>
    //             </div>             
    //     `;
    //     productList.appendChild(element);
    // }

    addProduct(product) {
        const productList = document.getElementById("prod-list");
        const element = document.createElement('tr');
        // element.className = "card text-center md-12 mt-2"
        element.innerHTML = `
                    <tr class="nombre_prod">
                        <td data-name="${product.name}">${product.name}</td>
                        <td>
                            <div class="quantity" >
                              <input type="number" min="1" value="${product.quantity}">
                            </div>
                            <div class="quantity-nav">
                                <button type="button" class="quantity-button quantity-up" name="qtn-up">+</button>
                                <div type="button" class="quantity-button quantity-down" name="qtn-down">-</div>
                            </div>
                        </td>
                        <td>${product.price}</td>
                        <td id="total">
                            <input type="number" class="total" value="${product.total}" readonly>
                        </td>
                        <td>
                            <a href="#" class="btn btn-danger" name="delete">Delete</a>
                        </td>
                    </tr>
            
        `;
        productList.appendChild(element);
    }

    resetForm() {
        document.getElementById('product-form').reset();
    }

    delete_row_prod(element) {
        element.remove();
        this.showMessage('Product Deleted Succsssfully', 'info');
        // if (element.name === 'delete') {
        // }
    }

    showMessage(message, cssClass) {
        const div = document.createElement('div');
        div.className = `alert alert-${cssClass} mt-2`;
        div.appendChild(document.createTextNode(message));
        // Show in The DOM
        const container = document.querySelector('.container');
        const app = document.querySelector('#App');
        // Insert Message in the UI
        container.insertBefore(div, app);
        // Remove the Message after 3 seconds
        setTimeout(function () {
            document.querySelector('.alert').remove();
        }, 3000);
    }
}

// DOM Events
let formulario = document.getElementById('product-form');

// console.log(formulario);
// let boton_agregar = document.getElementById('agregar_producto');

// let celda_total = document.getElementById('acc_total');

LIMITE = 500000;
cantidad_inicial = 0;
let productos = [];
index_seleccionado = 0;
// index_delete = 0;
const suma_productos = arr => arr.map(producto => producto.total)
    .reduce((a, b) => parseFloat(a) + parseFloat(b), 0);

const producto_actual = (arr, sel) => arr.map((producto, i) => {

    if (producto.name === sel) {
        // prod_seleccionado = { ...productos[i] };
        index_seleccionado = i;
        // cantidades[i];
        // cantidades[i] += 1;
        // console.log(cantidades[i]);
    }
});

// console.log(productos);
let nom_prod, curr_prod_price, curr_prod_cantidad, curr_prod_total;

min_value = 0;

btn = document.querySelector("button");

formulario.addEventListener('click', (e) => {

    nom_prod_dom = e.target.parentElement.parentElement.parentElement.firstElementChild.dataset['name'];
    total_prod_dom = e.target.parentElement.parentElement.parentElement;
    nom_del_row = e.target.parentElement.parentElement.firstElementChild.dataset['name'];
    del_row_dom = e.target.parentElement.parentElement;
    // precio_dom = e.target.parentElement.parentElement.parentElement.children[2].dataset['precio'];
    // precio_dom = e.target.parentElement.parentElement.parentElement.children[2];
    // console.log(e.target);

    const ui = new UI();
    const name = document.getElementById('name').value,
        price = document.getElementById('price').value,
        quantity = document.getElementById('quantity').value
    total = parseFloat((parseFloat(price) * parseInt(quantity))).toFixed(2);


    cantidad_elemento = document.getElementById('acumulado');
    // td = document.getElementsByTagName('td');
    enviar_orden = document.getElementById('enviar');

    btn_agregar =
        e.target.name === 'agregar'
            ? (
                (name === '' || price === '' || quantity === '')
                    ? (
                        e.target.disabled = true,
                        ui.showMessage('Please Insert data in all fields', 'danger'),
                        e.target.disabled = false
                    )
                    : (
                        producto = new Product(name, parseFloat(price), parseInt(quantity), parseFloat(total).toFixed(2)),
                        (
                            (producto.total <= LIMITE)
                                ? (
                                    ui.addProduct(producto),
                                    ui.resetForm(),
                                    // console.log(e.target),
                                    cantidad_inicial = producto.quantity,
                                    productos.push(producto),

                                    cantidad_elemento.value = suma_productos(productos),
                                    ui.showMessage('Product Added Successfully', 'success')
                                )
                                : ""
                        ),
                        (
                            (suma_productos(productos) >= LIMITE)
                                ? btn.setAttribute("disabled", "")
                                : (
                                    // cantidad_elemento.value = 0,
                                    ui.showMessage('Tu orden sobrepasa el limite', 'warning')
                                )
                        )
                    )
            )
            :
            ""
    // console.log(producto);
    btn_qt = document.getElementsByClassName('quantity');


    btn_quantity =
        e.target.className === 'quantity-button quantity-up'
            ? (
                // console.log(btn_incr[0].attributes[3].value),
                producto_actual(productos, nom_prod_dom),
                // console.log(index_seleccionado),
                { name: nom_prod, price: curr_prod_price, quantity: curr_prod_cantidad, total: curr_prod_total } = productos[index_seleccionado],

                curr_prod_cantidad += 1,
                curr_prod_total = curr_prod_price * curr_prod_cantidad,

                productos[index_seleccionado] = { ...productos[index_seleccionado], quantity: curr_prod_cantidad, total: curr_prod_total },
                // console.log(productos[index_seleccionado])
                // curr_prod_total = cantidades[index_seleccionado] * productos[index_seleccionado].price,


                btn_qt[index_seleccionado].firstElementChild.value = curr_prod_cantidad.toString(),
                // // console.log(total_prod_dom.children[3].children[0].value)
                total_prod_dom.children[3].children[0].value = curr_prod_total.toFixed(2),

                cantidad_elemento.value = suma_productos(productos),

                (
                    (cantidad_elemento.value >= LIMITE.toString())
                        ? (
                            btn.setAttribute("disabled", ""),
                            // console.log(cantidad_elemento.value),

                            // e.target.attributes[3].value = true,
                            // btn_incr[0].attributes[3].value = true,
                            // console.log(e.target.attributes[3].value),
                            // curr_prod_total = LIMITE,
                            // curr_prod_cantidad = curr_prod_total / curr_prod_price,
                            // productos[index_seleccionado] = { ...productos[index_seleccionado], quantity: curr_prod_cantidad, total: curr_prod_total },
                            // total_prod_dom.children[3].children[0].value = curr_prod_total.toFixed(2),
                            // cantidad_elemento.value = suma_productos(productos),
                            ui.showMessage('Haz alcanzado tu límite de compras', 'danger')
                        )
                        : ""
                )
                // (value <= max_value)
                //     ? (
                //         quantity[0].firstElementChild.value = value.toString()
                //         // e.target.attributes.disabled.value = true
                //     )
                //     :
                //     value = max_value,

            )
            : (
                e.target.className === 'quantity-button quantity-down'
                    ? (
                        // e.target.attributes.disabled.value = true,
                        // console.log(e.target),
                        producto_actual(productos, nom_prod_dom),
                        { name: nom_prod, price: curr_prod_price, quantity: curr_prod_cantidad, total: curr_prod_total } = productos[index_seleccionado],

                        curr_prod_cantidad -= 1,
                        curr_prod_total = curr_prod_price * curr_prod_cantidad,

                        productos[index_seleccionado] = { ...productos[index_seleccionado], quantity: curr_prod_cantidad, total: curr_prod_total },

                        btn_qt[index_seleccionado].firstElementChild.value = curr_prod_cantidad.toString(),
                        total_prod_dom.children[3].children[0].value = curr_prod_total.toFixed(2),

                        cantidad_elemento.value = suma_productos(productos),


                        (
                            (curr_prod_cantidad <= min_value)
                                ? (
                                    curr_prod_cantidad = min_value,
                                    curr_prod_total = min_value,
                                    productos[index_seleccionado] = { ...productos[index_seleccionado], quantity: curr_prod_cantidad, total: curr_prod_total },
                                    btn_qt[index_seleccionado].firstElementChild.value = min_value.toString(),
                                    total_prod_dom.children[3].children[0].value = curr_prod_total.toFixed(2),
                                    cantidad_elemento.value = suma_productos(productos),
                                    ui.showMessage('Desea eliminar este producto', 'danger')
                                )
                                : ""
                        ),
                        ( // Con este codigo se habilita el boton quantity-up, removiendo el atributo disabled
                            (btn.attributes[3] && enviar_orden.attributes[4] && (cantidad_elemento.value <= LIMITE.toString()))
                                ?
                                (
                                    btn.removeAttribute("disabled"),
                                    enviar_orden.removeAttribute("disabled")
                                    // console.log(btn_incr.attributes)
                                )
                                : ""
                        )




                    )
                    : ""
            )

    const btn_delete = e.target.name === 'delete'
        ? (
            producto_actual(productos, nom_del_row),
            // producto_actual(productos, nom_prod_dom),
            // console.log(producto_actual(productos, ix_del_row)),
            // console.log(del_row_dom),
            // index_seleccionado = ix,
            // deleteProduct(productos, index_seleccionado),
            // console.log(index_seleccionado),
            ui.delete_row_prod(del_row_dom),
            productos.splice(index_seleccionado, 1),
            cantidad_elemento.value = suma_productos(productos),

            ( // Con este codigo se habilita el boton quantity-up, removiendo el atributo disabled
                (btn.attributes[3] && enviar_orden.attributes[4] && (cantidad_elemento.value <= LIMITE.toString()))
                    ?
                    (
                        btn.removeAttribute("disabled"),
                        enviar_orden.removeAttribute("disabled")
                        // console.log(btn_incr.attributes)
                    )
                    : ""
            )

            // producto_actual(productos, nom_prod_dom),
            // productos[index_seleccionado] = { ...productos[index_seleccionado], quantity: 0, total: 0 }
            // console.log(deleteProduct(productos, name_del_row)),
            // console.log(productos),
            // console.log(productos.length),
            // console.log(index_delete),
            // console.log(index_seleccionado),
            // console.log(productos)
            // console.log(productos)
        )
        : (
            ""
        )

    const enviar =
        // console.log(enviar_orden);
        // enviar_orden.attributes[4]
        (
            cantidad_elemento.value >= LIMITE.toString()
                ? enviar_orden.setAttribute("disabled", "")
                : ""
        )

    e.preventDefault();
})

// console.log();
// total_prod = document.getElementById('total')


// total_prod.innerHTML = total

// console.log(total_prod.innerHTML)


// console.log(total);
// Create a new Oject Product
// console.log(typeof (total))

// total_prod.innerHTML = product.total
// Create a new UI
// const ui = new UI();
// console.log();
// btn_del = (e.target.name === 'delete')
//     ? ui.deleteProduct(e.target)
//     : console.log('hola');
// valor_total_actual = parseFloat(celda_total.innerHTML.replace("$ ", ""));

// if 
// } else {
//     // boton_agregar.disabled = false;
//     ui.addProduct(product);
//     ui.showMessage('Product Added Successfully', 'success');

//     acc_total_arr.push(product.total);
//     const sum = arr => arr.reduce((a, b) => parseFloat(a) + parseFloat(b), 0);
//     const ac_total = sum(acc_total_arr).toFixed(2);
//     celda_total.innerHTML = `$ ${ac_total}`

//     valor_total_actual = parseFloat(ac_total);

//     if (valor_total_actual >= LIMITE) {
//         ui.addProduct(product);
//         ui.showMessage('Ha superado el límite de su compra', 'warning');
//         celda_total.innerHTML = `$ ${ac_total}`
//         boton_agregar.disabled = true;
//     }
// }

// product.ajuste_limite(acumulado);
// console.log(ui.accum_total(acumulado.toString()))

// datos = new FormData(formulario);
// console.log(datos)
// console.log(datos.getAll('quantity'))
// console.log(datos.get('quantity'));
// console.log(datos.get('name'));
// console.log(datos.get('price'));


// fetch('http://127.0.0.1:53965/api/1.0/guardar_orden_item', {
//     method: 'POST',
//     body: datos
// }).then(res => res.json())
//     .then(data => data)



// let prod_list = document.getElementById('prod-list');

// prod_list.addEventListener('click', function (e) {
//     const ui = new UI();
//     // console.log(e.target);

//     // if (Array.isArray(acc_total_arr) && acc_total_arr.length) {
//     //     let str = e.target.parentElement;
//     //     let sum = arr => arr.reduce((a, b) => parseFloat(a) + parseFloat(b), 0);
//     //     acc = sum(acc_total_arr);
//     //     const total_seleccionado = parseFloat(str.dataset['total']);
//     //     acc_total_arr.map((i, ix) => {
//     //         if (total_seleccionado === parseFloat(i)) {
//     //             num = acc_total_arr.splice(ix, 1);
//     //             acc = acc - num[0];
//     //             if (acc <= LIMITE) {
//     //                 boton_agregar.disabled = false;
//     //             }
//     //             let acc_str = acc.toFixed(2);
//     //             celda_total.innerHTML = `$ ${acc_str}`;
//     //             const ui = new UI();
//     //             ui.deleteProduct(e.target);
//     //         }
//     //     })
//     //     e.preventDefault();
//     // }
// });

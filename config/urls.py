"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('usuarios/', include('apps.usuarios.urls', namespace='usuarios')),
    path('productos/', include('apps.productos.urls', namespace='productos')),
    path('order/', include('apps.order.urls', namespace='order')),
    path('orderuser/', include('apps.orderUser.urls', namespace='orderUser')),
    path('orderitem/', include('apps.orderItem.urls', namespace='orderItem')),
    path('api/1.0/guardar_orden_item', include('apps.orderItem.urls', namespace='guardar_orderItem')),
]
